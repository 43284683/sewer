import sbt._
import Keys._

import java.io.File

object Sewer extends Build {

  lazy val projectSettings = Defaults.defaultSettings ++ Seq(
    name := "Sewer for Parleys Videos",
    version := "0.0.1",
    organization := "net.oschina.oldpig.sewer",
    scalaVersion := "2.10.2",
    resolvers ++= Seq(
      "snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
      "releases" at "http://oss.sonatype.org/content/repositories/releases"
    ),
    scalacOptions ++= Seq("-deprecation", "-unchecked"),
    javaSource in Compile <<= baseDirectory(_ / "src"),
    scalaSource in Compile <<= baseDirectory(_ / "src"),
    scalaSource in Test <<= baseDirectory(_ / "test"),
    javaHome := Some(new File(System.getenv("JAVA_HOME"))),
    unmanagedJars in Compile <+= javaHome map { jh: Option[File] =>
      Attributed.blank(new File(jh.get, "/jre/lib/jfxrt.jar"))
    },
    fork in run := true,
    libraryDependencies ++= {
      Seq(
        "org.scalaz" %% "scalaz-core" % "7.0.0",
        "org.scalatest" %% "scalatest" % "1.9.1" % "test",
        "org.scalafx" %% "scalafx" % "1.0.0-M4",
        "com.typesafe" %% "scalalogging-log4j" % "1.1.0-SNAPSHOT" notTransitive(),
        "net.liftweb" %% "lift-json" % "2.5.1",
        "org.apache.logging.log4j" % "log4j-api" % "2.0-beta7",
        "org.apache.logging.log4j" % "log4j-core" % "2.0-beta7",
        "com.google.guava" % "guava" % "14.0.1"
      )
    }
  )


  lazy val root = Project(id = "Sewer", base = file("."), settings = projectSettings)
}

