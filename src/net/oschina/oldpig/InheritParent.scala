package net.oschina.oldpig

import javafx.scene.{Node, Parent}

object InheritParent {
  implicit class SParent[T <: Parent](parent: T) extends Parent {
//        override protected def getChildren = parent.getChildren

    def apply(children: Seq[Node]) = {
      println(children)
      parent.getChildren.addAll(children: _*)
      println(parent.getChildren)
      parent
    }
  }

}
