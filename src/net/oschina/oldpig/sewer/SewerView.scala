package net.oschina.oldpig.sewer

import javafx.scene.media.MediaView
import javafx.scene.layout.{Region, Priority, HBox, BorderPane}
import javafx.scene.image.{Image, ImageView}
import javafx.geometry.{Pos, Insets}
import javafx.scene.control._
import javafx.event.{ActionEvent, EventHandler}
import javafx.scene.input.MouseEvent
import javafx.scene.Cursor
import javafx.beans.value.{ObservableValue, ChangeListener}
import javafx.stage.FileChooser

trait SewerView extends BorderPane with SewerController{
  val mediaView = new MediaView
  val slideView = new ImageView
  val contentPane = new HBox(5)
  val mediaBar = new HBox(8)
  val imageViewPlay = new ImageView(new Image(this.getClass.getResourceAsStream("/playbutton.png")))
  val imageViewPause = new ImageView(new Image(this.getClass.getResourceAsStream("/pausebutton.png")))
  val playButton = new Button
  val progressBar = new ProgressBar
  val playTime = new Label("PlayTime")
  val loadButton = new Button
  val volumeLabel = new Label("Vol")
  val volumeSlider = new Slider


  contentPane.getChildren.addAll(mediaView, slideView)
  contentPane.setStyle("-fx-background-color: black;")
  mediaBar.setPadding(new Insets(5, 10, 5, 10))
  mediaBar.setAlignment(Pos.CENTER_LEFT)
  BorderPane.setAlignment(mediaBar, Pos.CENTER)
  // playButton
  playButton.setMinWidth(Control.USE_PREF_SIZE)
  playButton.setGraphic(imageViewPlay)
  playButton.setOnAction(new EventHandler[ActionEvent]() {
    def handle(p1: ActionEvent) {
      onPlayOrPause()
    }
  })
  // progressBar
  progressBar.setMinWidth(30)
  progressBar.setMaxWidth(Double.MaxValue)
  HBox.setHgrow(progressBar, Priority.ALWAYS)
  progressBar.setOnMousePressed(new EventHandler[MouseEvent] {
    def handle(e: MouseEvent) {
      onSeek(e.getX)
    }
  })
  progressBar.setCursor(Cursor.TEXT)
  // playTime
  playTime.setMinWidth(Control.USE_PREF_SIZE)
  // loadButton
  loadButton.setText("Load Package")
  loadButton.setMinWidth(Control.USE_PREF_SIZE)
  loadButton.setOnAction(new EventHandler[ActionEvent] {
    def handle(e: ActionEvent) {
      onLoad
    }
  })
  // volumeLabel

  volumeLabel.setMinWidth(Control.USE_PREF_SIZE)
  // volumeSlider
  volumeSlider.setPrefWidth(70)
  volumeSlider.setMinWidth(30)
  volumeSlider.maxWidth(Region.USE_PREF_SIZE)
  volumeSlider.setMax(100)
  volumeSlider.valueProperty().addListener(new ChangeListener[Number] {
    def changed(p1: ObservableValue[_ <: Number], oldValue: Number, newValue: Number) {
      onVolume(newValue)
    }
  })
  mediaBar.getChildren.addAll(playButton, progressBar, playTime, loadButton, volumeLabel, volumeSlider)

  override def layoutChildren() {
    if (mediaView != null && this.getBottom != null) {
      mediaView.setFitWidth(this.getWidth - slideView.getFitWidth)
      mediaView.setFitHeight(this.getHeight - getBottom.prefHeight(-1))
    }
    super.layoutChildren()
    //    if (mediaView != null && this.getCenter != null) {
    //      mediaView.setTranslateX((getCenter.asInstanceOf[Pane].getWidth - mediaView.prefWidth(-1)) / 2)
    //      mediaView.setTranslateY((getCenter.asInstanceOf[Pane].getHeight - mediaView.prefHeight(-1)) / 2)
    //    }
  }

  setCenter(contentPane)
  setBottom(mediaBar)
  setMinSize(1194, 414)

}

object SewerView extends SewerView
