package net.oschina.oldpig.sewer

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.layout.{BorderPane, HBox}
import scalafx.scene.media.MediaView

class SewFx extends JFXApp {
  stage = new PrimaryStage {
    scene = new Scene {
      root = new BorderPane {
        center = new HBox{
          content = Seq(
            new MediaView()

          )
        }
      }
    }

  }

}
