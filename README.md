#Sewer

Sew video and slides together. Specially created for watching downloaded parleys.com videos and slides.

1. Download pre-packaged video-slide resources (`.sew` files) in QQ group #231809997

2. Clone me and run in sbt (require scala 2.10.x and correctly **JAVA_HOME** system environment variable set, pointing to oracle jdk 7u6 or above). 
	
   Or `sbt gen-idea` and run SewerMain in IDEA.

3. Press `Load Package` button and open a `.sew` file. Have fun.

4. Tested under Mac OS X and Windows XP with Divx Plus Codec installed. Should work under Win7 without extra lib but not work under Ubuntu 13.04.

5. In my test, JavaFX MediaPlayer sometimes crash the jvm silently on Window XP, this does not show up everytime.


 
 